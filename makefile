all : main.pdf

main.pdf : main.md
		pandoc 'main.md' -o docker-basics.pdf --pdf-engine=lualatex -t beamer
