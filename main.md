---
title: Docker basics

lang: "fr-FR"
author: [Rémi Teissier]
date: \today

topic:
  - docker
  - container

keywords: [docker, container]

pagenumbering: arabic
toc: true
theme: Boadilla
# colorscheme: crane
# colorscheme: wolverine

colorlinks: true
linkcolor: red
contrastcolor: red
linkstyle: bold

aspectratio: 169

fontsize: 10pt
mainfont: FiraCodeNerdFont

...

# Prérequis

Installé :

- `Docker`
- `docker-compose`
- `mongo-compass`

---

# What ?

::: columns
:::: {.column width=40%}
![Classic VM](./asset/VM-architecture.png)
:::: 

. . .

:::: {.column width=40%}
![Docker](./asset/docker-architecture.png){heigth=50%}
::::
:::

. . .

- Bye bye `"Guest" OS n` -> "Just" an executable
- **ATTENTION** : Docker != virtualisation

---

# Pourquoi ?

. . .

- Mise en place + facile :
    - qu'une VM
    - qu'un service sur sa distro
- Exécute indépendemment des OS :
    - Cross-plateform
    - Utilisable en CI/CD
- Éviter le [Cattle vs Pets problem](https://www.hava.io/blog/cattle-vs-pets-devops-explained)

---

# Le container

## Différents éléments

::: columns
:::: {.column width=70%}
![Docker architecture](./asset/docker-registry.png){height=60%}
::::

:::: {.column}
Docker :

- != Container
- != Image
- != Volume
- != Pod


Registry :

- Docker Hub
- Aws
- Gitlab
- Github
- ...
::::
:::

---

## Exercice 1

1. `pull` explicitement l'image mongo depuis docker hub
2. **Créer** un container faisant tourner mongo avec :
    - Le user principal définit à `root`
    - Le mot de passe définit à `root`
    - Accessible sur le port `27099` au lieu de `27017`
    - Donner le nom `my-mongo` au container
3. Run `docker container ls` puis `docker container ls --all`
4. `start` le container


Ref :

- [Doc `docker create`](https://docs.docker.com/engine/reference/commandline/container_create/)
- [Mongo Docker hub](https://hub.docker.com/_/mongo/)

. . .

```shell
docker container create \
    -e MONGO_INITDB_ROOT_USERNAME=root \
    -e MONGO_INITDB_ROOT_PASSWORD=root \
    --publish 27099:27017 --name my-mongo mongo
docker start my-mongo
```


---

## Lifecycle d'un container

![Lifecycle](./asset/docker-container-lifecycle-diagram.png){height=70%}

---

## Exercice 2

1. Accéder au mongo depuis l'url `mongodb://root:root@localhost:27099`
2. Jouer avec (ajout de document...)
3. `stop`, `start`
4. `stop` et suppression du container
5. Recréation et redémmarage

. . .

Leçons :

- Pas de persistence entre container
- Techniquement pas perdu `sudo ls -l /var/lib/docker/volumes`

```shell
docker stop my-mongo
docker start my-mongo
# Check on mongo compass
docker stop my-mongo
docker rm my-mongo
docker container run \
    -e MONGO_INITDB_ROOT_USERNAME=root \
    -e MONGO_INITDB_ROOT_PASSWORD=root \
    --publish 27099:27017 --name my-mongo mongo
```

---

# Persistance de données

## Volumes

- Se "monte" n'importe ou dans le container
- Peut être un volume abstrait docker ou un dossier local
- Commandes accessible via `docker volume`

---

## Exercice 3

1. `stop` et `rm` tous les container
2. Créer un volume `mongo-data`
3. Afficher la liste de tous les volumes
4. Créer et lancer notre container mongo avec le volume attacher
5. Enregistrer de la donnée
6. Éliminer tous les volumes non utilisés
7. `stop`, `rm` et recréation du container

---

## Exercice 3

```shell
docker stop my-mongo ; docker rm my-mongo
docker volume create mongo-data
docker volume ls
docker container run
    -e MONGO_INITDB_ROOT_USERNAME=root \
    -e MONGO_INITDB_ROOT_PASSWORD=root \
    --publish 27099:27017 --name my-mongo \
    --volume mongo-data:/data/db mongo
# See et manipule la donnée
docker stop my-mongo ; docker rm my-mongo
docker container run \
    -e MONGO_INITDB_ROOT_USERNAME=root \
    -e MONGO_INITDB_ROOT_PASSWORD=root \
    --publish 27099:27017 --name my-mongo \
    --volume mongo-data:/data/db mongo
docker volume prune
docker stop my-mongo ; docker rm my-mongo
```

---

# Container as Cattle

## Problème : Assuré la reproductibilité

- CLI fastidieux
- Pas d'automatisation

Solution ?

. . .

Script automatisé ?

---

## Docker compose

![Docker Compose plusieurs services](./asset/docker-compose.png){height=80%}

---

## Exercice 4

Exercice 3 mais avec un `docker-compose.yml`

---

# Créer une image

## Les phases de build et d'éxécution

![Build part](./asset/docker_environment_build_args_overview.png){width=80%}

---

## Layers d'un `Dockerfile`

![Layers](./asset/image-layers.png)

- [Exemple sur `python` avec le tag `latest`](https://hub.docker.com/layers/library/python/latest/images/sha256-374e74dd816f68285aee7c255308829df53349134dc42c42908b69e301324c2c?context=explore)
- Commandes accessible via `docker volume`


---

## Exercice 5

```shell
mkdir -p build-docker-test ; cd build-docker-test 
echo 'print("Hello world from my app")' > main.py
```

1. Créer un Dockerfile pour lancer cette "application" avec pour nom `local/my-hello`
2. `run` un container pour print "Hello world from my app"
3. Detruire tous les container

- [Dockerfile documentation](https://docs.docker.com/engine/reference/builder/)

---

## Exercice 6

1. Modifier le code pour afficher `Hello <WHO>` ou `<WHO>` correspondra à la variable d'environnement `WHO`
2. `run` pour que l'on ai d'afficher `Hello from docker`

---

# Dev avec `build` et `compose`

`compose` permet aussi de déveloper et partager du code en dev plus facilement entre les différents programmeurs.

Pour ça, il va falloir builder facilement notre projet en plus de le relier au potentiels services.

---


## Exercice 7

1. Rendez-vous dans le projet `api`
2. Créez un `docker-compose` avec mongo (comme avant)
3. Définir le `build` et l'exécution de l'api utilisant le mongo **seulement** avec le `docker compose up`
4. Faire en sorte que le mongo ne soit accessible **que** par l'api (pas par le host)

- [Doc `build` dans un `compose`](https://docs.docker.com/compose/compose-file/build/)

---

# Idée reçues

- Docker != container
- != Replication complète : see nix/guix
- Docker != virtualisation et isolation complète

---

# Autour de docker

- Standard par l'usage avec l'Open Container Initiative:
    - [Open Container Initiative](https://opencontainers.org/)
    - [Demystifying the Open Container Initiative (OCI) Specifications](https://www.docker.com/blog/demystifying-open-container-initiative-oci-specifications/)
- Alternative avec exactement le même CLI :
    - [Podman](https://podman.io/)
    - [Rkt](https://github.com/rkt/rkt) (discontinued)

![Logo podman](./asset/podman.png){height=50%}

---

# Conclusion

Vous devriez connaitre :

- Le fonctionnement de docker
- La dynamique entre les ressources :
    - Image
    - Registry
    - Container
    - Volume
- L'utilisation et la manipulation de ces ressource par CLI
- Le build d'image avec le `Dockerfile`
- Par `compose` :
    - La définition statique de services
    - L'interconnection entre plusieurs services
    - Le build
